(ns chem100.prototype.parsetree-test
  (:use clojure.test
        chem100.prototype.parsetree))

(parse-tree "connect to http://localhost:8080/alfresco/cmisatom user admin password admin")
(parse-tree "connect to http://localhost:8080/alfresco/cmisatom user admin password admin as dev")

(parse-tree "update dpp:person set dpp:middleName = 'Maybe', dpp:firstName = 'mark'")
(parse-tree "update dpp:person set dpp:size = 123 ")

(parse-tree "select  dpp:foo, dpp:bar from  dpp:table")

(parse-tree "insert file into path '/' (cmis:name,cmis:objectTypeId,filename) values ('testFolder','dpp:person','test.pdf')")
(parse-tree "insert document into path '/A Test Folder' (filename) values ('hello world.pdf')")
(parse-tree "insert folder into path '/A Test Folder' (filename) values ('hello world.pdf')")
(parse-tree "insert file into path '/A Test Folder' (filename) values ('hello world.pdf')")
(parse-tree "insert link into path '/A Test Folder' (filename) values ('hello world.pdf')")
(parse-tree "insert document into objectId 1234-1234-1234 (filename) values ('hello world.pdf')")

(parse-tree "delete from dpp:article")
