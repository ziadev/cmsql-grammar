(ns chem100.prototype.ql-test
  (:use clojure.test
        chem100.prototype.ql)
  (:require [chem100.session :as session]))

(connect "connect to http://localhost:8080/alfresco/cmisatom user admin password admin")
;(connect "connect to http://localhost:8080/alfresco/cmisatom user admin password admin" "alfresco")
(deref session_map)

;; INSERT
;(exec-insert "insert file into path '/' (cmis:name, cmis:objectTypeId, filename) values ('purchase_order1.pdf', 'dpp:person', 'purchase_order1.pdf')")
;(exec-insert "insert file into path '/A Testing Folder' (cmis:name, cmis:objectTypeId, filename, mimeType) values ('purchase_order1.pdf', 'cmis:document', 'purchase_order1.pdf', 'application/pdf')")
;(exec-insert "insert file into path '/A Testing Folder' (cmis:name, cmis:objectTypeId, filename, mimeType) values ('project.clj', 'cmis:document', 'project.clj', 'text/plain')")
;(exec-insert "insert file into path '/A Testing Folder' (cmis:name, cmis:objectTypeId, filename, mimeType) values ('Stang.jpg', 'cmis:document', '/home/mstang/Downloads/Stang.jpg', 'image/jpeg')")
;(exec-select "select cmis:name from dpp:person")
(exec-select "select cmis:objectId from dpp:person")

;(. (exec-insert "insert folder into path / (cmis:name,cmis:objectTypeId) values (ATesting,cmis:folder)") delete)

;(exec-insert "insert folder into path '/' (cmis:name,cmis:objectTypeId) values ('A Testing Folder','cmis:folder')"
;(exec-insert "insert folder into path '/A Testing Folder' (cmis:name, cmis:objectTypeId, dpp:collectionType) values ('ATestFolder2', 'F:dpp:collection' , 'productApproved')")
;(exec-insert "insert folder into path '/A Testing Folder' (cmis:name, cmis:objectTypeId, dpp:collectionType) values ('Test', 'F:dpp:collection', 'productUnapproved')")
;(exec-insert "insert document into path '/A Testing Folder' (cmis:name, cmis:objectTypeId) values ('Hello World', 'cmis:document')")
;(exec-insert "insert document into path '/A Testing Folder' (cmis:name, cmis:objectTypeId, dpp:personId) values ('HelloPerson', 'D:dpp:person', 'mstang@ziaconsulting.com')")
;(exec-insert "insert document into path '/A Testing Folder/Test' (cmis:name, cmis:objectTypeId) values ('Hello World', 'cmis:document')")
;(exec-insert "insert document into path '/A Testing Folder/Test1' (cmis:name, cmis:objectTypeId, dpp:personId) values ('HelloPerson', 'D:dpp:person', 'mstang@ziaconsulting.com')")

; live-interactive objects
;;(folder/get-children (folder/get-root-folder (:default @session_map)))
;(doc/create-document (co/get-object-by-path (:default @session_map) "/ATestFolder") "hello world")

;; SELECT
;(pp (exec-select "select dpp:personId from dpp:person where dpp:lastName = 'Stang'" "alfresco"))
;(pp (exec-select "select dpp:lastName from dpp:person where dpp:lastName = 'Stang'"))
;(pp (exec-select "select dpp:personId, cmis:objectId,dpp:firstName, dpp:lastName from dpp:person where dpp:personId = 'mstang@ziaconsulting.com' or dpp:firstName = 'Mark'"))
;(println (exec-select "select dpp:personId, cmis:objectId,dpp:firstName, dpp:lastName from dpp:person where dpp:personId = 'mstang@ziaconsulting.com' or dpp:firstName = 'Mark'"))
;(exec-select "SELECT dpp:manuscriptNumber FROM dpp:article where dpp:manuscriptNumber like '123%'")
;(flatten (map vals (exec-select "select cmis:path from cmis:folder where cmis:name = 'Evaluation'" "qa")))

;(flatten (map vals (exec-select "select dpp:manuscriptNumber from dpp:article")))
;(join ", " (flatten (map vals (exec-select "select dpp:manuscriptNumber from dpp:article"))))
;(count (map vals (exec-select "select dpp:manuscriptNumber from dpp:article")))
;(pp (map vals (exec-select "select cmis:path from dpp:article")))
;(pp (sort-by last (map vals (exec-select "select cmis:path from dpp:article"))))

;(exec-select "select dpp:status, dpp:manuscriptNumber from dpp:article where cmis:name = '1189880'")
;(pp (exec-select "select cmis:objectId,cmis:path from dpp:collection"))
(exec-select "select cmis:objectId from dpp:article where dpp:manuscriptNumber = 1189880")
;(pp (map vals (exec-select "select cmis:contentStreamId, cmis:contentStreamLength, cmis:contentStreamFileName, alfcmis:nodeRef from dpp:supportingFile")))
;(pp (map vals (exec-select "select cmis:contentStreamFileName, alfcmis:nodeRef, cmis:creationDate from dpp:supportingFile")))

;; DELETE
;(defn create-folders []
;  (exec-insert "insert folder into path '/' (cmis:name,cmis:objectTypeId) values ('A Testing Folder','cmis:folder')")
;  (exec-insert "insert document into path '/A Testing Folder' (cmis:name, cmis:objectTypeId, dpp:personId, dpp:lastName, dpp:firstName) values ('HelloPerson', 'D:dpp:person', 'mstang@ziaconsulting.com', 'Stang', 'Mark')")
;  (exec-insert "insert folder into path '/A Testing Folder' (cmis:name, cmis:objectTypeId, dpp:collectionType) values ('Test1', 'F:dpp:collection', 'productUnapproved')")
;  (exec-insert "insert document into path '/A Testing Folder/Test1' (cmis:name, cmis:objectTypeId, dpp:personId, dpp:lastName, dpp:firstName) values ('HelloPerson', 'D:dpp:person', 'mstang@ziaconsulting.com', 'Stang', 'Mark')")
;  (exec-insert "insert folder into path '/A Testing Folder' (cmis:name, cmis:objectTypeId, dpp:collectionType) values ('Test', 'F:dpp:collection', 'productUnapproved')"))
;(create-folders)

;(exec-select "select dpp:personId,cmis:name from dpp:person where dpp:personId = 'mstang@ziaconsulting.com'")
;(exec-delete "delete from dpp:person where dpp:personId = 'mstang@ziaconsulting.com'")
;(exec-delete "delete folder from cmis:folder where cmis:name like 'A Testin%'")

;(exec-select "select cmis:name from cmis:folder where cmis:name like 'Test%'")
;(exec-delete "delete from cmis:folder where cmis:name = 'Test1'")

;(exec-select "select cmis:name from cmis:folder where cmis:name = 'Test1'")
;(exec-delete "delete from cmis:folder where cmis:name = 'Test'")
;(exec-delete "delete folder from cmis:folder where cmis:name = 'Test'")

;(exec-delete "delete folder from cmis:folder where cmis:name like 'Test%'")

;(exec-select "select cmis:name from cmis:folder where cmis:name like 'Test%'")

;(exec-delete "delete folder from path '/A Testing Folder'")
;(exec-delete "delete folder from path '/A Testing Folder/Test'")
;(exec-delete "delete from path '/A Testing Folder/HelloPerson'")
;(exec-delete "delete from path '/A Testing Folder/Test1/HelloPerson'")



;(exec-insert "insert folder into path '/' (cmis:name,cmis:objectTypeId) values ('A Testing Folder','cmis:folder')")
;(exec-insert "insert document into path '/A Testing Folder' (cmis:name, cmis:objectTypeId, dpp:personId, dpp:lastName, dpp:firstName) values ('HelloPerson', 'D:dpp:person', 'mstang@ziaconsulting.com', 'Stang', 'Mark')")
;(exec-select "select dpp:personId,cmis:name from dpp:person where dpp:personId = 'mstang@ziaconsulting.com'")
;(exec-delete "delete from dpp:person where dpp:personId = 'mstang@ziaconsulting.com'")

;(exec-delete "delete folder from cmis:folder where cmis:name like 'Test%'")

;(exec-insert "insert folder into path '/A Testing Folder' (cmis:name, cmis:objectTypeId, dpp:collectionType) values ('Test1', 'F:dpp:collection', 'productUnapproved')")
;(exec-select "select cmis:name from cmis:folder where cmis:name like 'Test%'")
;(exec-delete "delete from cmis:folder where cmis:name = 'Test'")

;(exec-delete "delete from cmis:document where cmis:name = 'Hello World'")

;(exec-insert "insert folder into path '/A Testing Folder' (cmis:name, cmis:objectTypeId, dpp:collectionType) values ('Test', 'F:dpp:collection', 'productUnapproved')")
;(exec-select "select cmis:name from cmis:folder where cmis:name = 'Test'")
;(exec-delete "delete from cmis:folder where cmis:name = 'Test'")
;(exec-delete "delete folder from cmis:folder where cmis:name = 'Test'")

;; UPDATE
;(exec-update "update dpp:person set dpp:middleName = 'Mayb not', dpp:firstName = 'Mark' where dpp:firstName = 'Mark'")
;(exec-select "select dpp:middleName,dpp:firstName from dpp:person where dpp:firstName = 'Mark'")

;; need to make sure we print the columns in the order listed
(defn pp [results-list]
  (loop [results-list results-list]
    (if (not (seq results-list)) nil
      (let [first (first results-list)]
        ;(println first)
        (recur (rest results-list))))))

