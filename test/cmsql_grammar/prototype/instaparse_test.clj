(ns chem100.prototype.instaparse-test
  (:use clojure.test
        chem100.prototype.instaparse)
  (:require [chem100.session :as session]))
(use '[clojure.string :only (join split)])

;; sql-parser
(sql-parser "test 1234")
(sql-parser "test 'abcd'")

(sql-parser "connect to http://localhost:8080/alfresco/cmisatom user admin password admin")

;; copy folder, copy from to; move xxx to
(sql-parser "copy path '/Editorial/2013/Jun/Original Submission/test.pdf' to path '/Editorial/2013/Jun/Evaluations'")

(sql-parser "update dpp:person set dpp:firstName = 'May' , dpp:lastName = 'Be' where dpp:personId = 'mstang@ziaconsulting.com'")

(sql-parser "insert document into path '/A Testing Folder' (cmis:name, cmis:objectTypeId, dpp:personId) values ('HelloPerson', 'D:dpp:person', 'mstang@ziaconsulting.com')")
(sql-parser "insert document into path '/A Test Folder' (filename) values ('hello world.pdf')")
(sql-parser "insert document into objectId workspace://SpacesStore/1234-12345-1245 (cmis:name, cmis:objectTypeId) values ('testFolder','dpp:person')")
(sql-parser "insert document into path '/' (cmis:name, cmis:objectTypeId, filename) values ('Test Folder','dpp:person','test.pdf')")
(sql-parser "insert file into path '/' (cmis:name,cmis:objectTypeId,filename) values ('testFolder','dpp:person','test.pdf')")


(sql-parser "select  dpp:foo, dpp:bar  from  dpp:table")
(sql-parser "select  dpp:foo, dpp:bar  from  dpp:table where dpp:name like '123%'")
(sql-parser "select dpp:foo from dpp:table where dpp:name = '123%'")
(sql-parser "select dpp:foo, dpp:bar from dpp:table where dpp:name != '5' or dpp:name = 6")
(sql-parser "select * from dpp:table")
(sql-parser "select * from dpp:table where dpp:name = '5'" )
(sql-parser "select count from dpp:table where dpp:name = 'hello world'" )

(sql-parser "delete from dpp:article where dpp:name = '5'")
(sql-parser "delete folder from dpp:article where dpp:name = 'hello world'")
(sql-parser "delete from dpp:article")

;; parse-statement
(parse-statement "delete from dpp:person")
(parse-statement "delete from path '/A Testing Folder'")
(parse-statement "select * from dpp:person")
(parse-statement "select count from dpp:person")
(parse-statement "update dpp:person set dpp:middleName = 'Maybe', dpp:firstName = 'mark' where dpp:firstName = 'Mark'")

;; build-hashmap
(build-hashmap (list "dpp:middleName" "Maybe" "dpp:firstName" "mark"))

;; parse-tree
(parse-tree "connect to http://localhost:8080/alfresco/cmisatom user admin password admin")

(parse-tree "update dpp:person set dpp:firstName = 'May' , dpp:lastName = 'Be', dpp:phone = 3034404645 where dpp:firstName = 'hello world'")
(parse-tree "update dpp:person set dpp:middleName = 'Maybe', dpp:firstName = 'mark' where dpp:firstName = 'Mark'")

(parse-tree "insert file into path '/' (cmis:name,cmis:objectTypeId,filename) values ('testFolder','dpp:person','test.pdf')")
(parse-tree "insert document into path / (cmis:name,cmis:objectTypeId,filename) values ('testFolder','dpp:person','test.pdf')")
(parse-tree "insert document into path / (cmis:name,cmis:objectTypeId,filename) values ('testFolder','dpp:person',123456789,987654321)")
(parse-tree "insert document into objectId workspace://SpacesStore/1234-12345-1245 (cmis:name,cmis:objectTypeId) values ('testFolder','dpp:person')")
(:values (parse-tree "insert document into path '/' (cmis:name,cmis:objectTypeId,filename) values ('test Folder','dpp:person','test.pdf')"))
(parse-tree "insert folder into path '/' (cmis:name,cmis:objectTypeId) values ('test.txt','dpp:person')")
(:values (parse-tree "insert document into path '/' (cmis:name) values (1234, 5678)"))
(:values (parse-tree "insert document into path '/' (cmis:name) values ('test Folder', 'filename')"))
(:values (parse-tree "insert document into path '/A Testing Folder' (cmis:name, cmis:objectTypeId, dpp:personId) values ('HelloPerson', 'D:dpp:person', 'mstang@')"))

(parse-tree "select dpp:foo, dpp:bar from dpp:table where dpp:name like '123%'")
(parse-tree "select dpp:lastName from dpp:person where dpp:firstName = 'hello world'")
(parse-tree "select dpp:lastName from dpp:person where dpp:firstName = 'hello world' or dpp:firstName = 'mark'")
(parse-tree "select dpp:lastName from dpp:person where dpp:firstName = 9")
(parse-tree "select * from dpp:person")
(parse-tree "select * from dpp:person where folder = 'Hello World'")
(parse-tree "select count from dpp:person")
(parse-tree "select dpp:name from dpp:person")
(parse-tree "select dpp:firstName, dpp:lastName from dpp:person")
(parse-tree "select dpp:lastName,dpp:firstName from dpp:person where dpp:firstName = 'bob' or dpp:lastName = 'smith'")
(parse-tree "select dpp:firstName, dpp:lastName from dpp:person where dpp:firstName = 'hello world'")
(parse-tree "select dpp:firstName, dpp:lastName from dpp:person where dpp:firstName = 123456789")

(parse-tree "delete from cmis:folder")
(parse-tree "delete from dpp:person")
(parse-tree "delete from dpp:person where x = 'bob'")
(parse-tree "delete from dpp:person where dpp:firstName = 'mark' or y = 6")

(parse-tree "delete from path '/A Testing Folder/HelloPerson'")
(parse-tree "delete folder from path '/A Testing Folder'")
(parse-tree "delete from cmis:folder where cmis:name = 'Test'")
(parse-tree "delete folder from cmis:folder where cmis:name = 'Test'")
(parse-tree "delete from cmis:folder where cmis:name = 'Test'")

(:where-clause (parse-tree "delete from dpp:person where dpp:firstName = 'hello world'"))
(:where-clause (parse-tree "delete from dpp:person where dpp:firstName = 1234567890"))

;; delete folder

(join " " (:where-clause (parse-tree "delete from dpp:person where dpp:firstName = 'mark' or y = 6")))

;; N.B.
;; implement parsing of describe

(parse-tree "describe ")

