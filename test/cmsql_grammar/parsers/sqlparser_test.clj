(ns cmsql-grammar.sqlparser-test
  (:require [instaparse.core :as insta])
  (:use clojure.test
        cmsql-grammar.parsers.sqlparser))

;; not supported
;(sql-parser "select dpp:foo, dpp:bar from dpp:table where dpp:name = dpp:firstName")

;; test samples, need actual tests
(sql-parser "select dpp:lastName from dpp:person where (dpp:firstName = 'hello world') or (dpp:firstName = 'mark')")
(sql-parser "select m.col1,m.col2 AS COL FROM table AS m where m.col1 = 'hello' ORDER BY m.COL1,  m.col2")
(sql-parser "select  dpp:foo, dpp:bar  from  dpp:table")
(sql-parser "select  dpp:foo, dpp:bar  from  dpp:table where dpp:name like '123%'")
(sql-parser "select dpp:foo from dpp:table where dpp:name = '123%'")
(sql-parser "select dpp:foo, dpp:bar from dpp:table where dpp:name != '5' or dpp:name = 6")
(sql-parser "select * from dpp:table")
(sql-parser "select * from dpp:table where dpp:name = '5'" )
(sql-parser "select count from dpp:table where dpp:name = 'hello world'" )
(sql-parser "select * from dpp:table where dpp:name = 'a'" )
(sql-parser "select dpp:foo, dpp:bar from dpp:table where dpp:name like '123%'")
(sql-parser "select dpp:lastName from dpp:person where dpp:firstName = 'hello world'")
(sql-parser "select dpp:lastName from dpp:person where dpp:firstName = 'hello world' or dpp:firstName = 'mark'")
(sql-parser "select dpp:lastName from dpp:person where dpp:firstName = 9")
(sql-parser "select * from dpp:person")
(sql-parser "select * from dpp:person where folder = 'Hello World'")
(sql-parser "select count from dpp:person")
(sql-parser "select dpp:name from dpp:person")
(sql-parser "select dpp:firstName, dpp:lastName from dpp:person")
(sql-parser "select dpp:lastName,dpp:firstName from dpp:person where dpp:firstName = 'bob' or dpp:lastName = 'smith'")
(sql-parser "select dpp:firstName, dpp:lastName from dpp:person where dpp:firstName = 'hello world'")
(sql-parser "select dpp:firstName, dpp:lastName from dpp:person where dpp:firstName = 123456789")

(sql-parser "SELECT Orders.OrderID, Customers.CustomerName, Orders.OrderDate FROM Orders JOIN Customers ON Orders.CustomerID = Customers.CustomerID;")

(sql-parser "SELECT cmis:name, job.sched:jobNotes, job.sched:jobTime from sched:jobFolder relate sched:appointmentToJobAssoc as job on sched:dayFolder")

(sql-parser "SELECT cmis:name, job.sched:jobNotes, job.sched:jobTime FROM sched:jobFolder AS jf RELATE sched:appointmentToJobAssoc AS job ON sched:dayFolder where jf.sched:scheduleYearMonthDay = '20140407'")
