Introducing the Content Management Structured Query Language (cmsql)
====
This is a fork of CHEM100.  CHEM100, originally, contained the Grammar Parser, the AST Parser and the execution code.  This contains the Grammar Parser and the AST Parser.  This enables us to reuse the parsers with different implementations of the Execution.   The output of the Parser is a Map.

The Grammar is based on SQL-92.  The CMIS contains the original description of the Grammar, which was extended to support Content Management.  The AST is built to turn the output of the Grammar Parser into a Map that specifies the results.  The Map can then be passed on to an execution process that implements the back-end logic.  The original implementation (a.k.a. prototype) used CMIS as the implementation.  By breaking this up, we can reuse the Parsers with different implementations (i.e. Lambdalf).

----

[LightTable](http://www.lighttable.com/)
----
Most of the modern IDE's have Lisp plugins (even VIM).   However, the level of integration with the REPL and the IDE varies.  With Light Table, the integration is almost seamless.  It is possible to use Eclipse, IntelliJ, Emacs and even Vi(m) to work with Clojure and the Content Management Structured Query Language, using Light Table makes it a lot easier to install and very little configuration is required.

Start by cloning this project, then install Light Table.  Once Light Table is running, select the 'View' Menu Item and then select 'Workspace'.  From there you can select the cloned version of cmsql-grammar.

Connecting to a cmsql-grammar REPL
----
A quick word of warning, anything in these files can be executed or an entire file can be executed.  If there is a connection to a Repository and a delete, it is entirely possible to delete entire trees of folders.  So with great power, comes great responsibility.

The Light Table IDE can connect up to a running REPL or you can have it start one.  From then on, the code/commands are sent to the REPL, executed and the response returned.  To execute a single form, put your cursor anywhere in the form and hit <CTRL-ENTER>.  Beware, if you select <SHIFT-CTRL-ENTER> then the entire file is processed.

----
Everything below this needs to be reviewed...Take it with a very large grain of salt.

The easiest way to create/connect to a REPL is to put your cursor in the first line of the demo001.clj file:

chem100/src/chem100/alfrescoSummit2013/demo001.clj

(ns chem100.alfrescoSummit2013.demo001)

And press <CTRL-ENTER>.  If you watch the bottom of the screen, you will see Light Table create and connect to a REPL.
From then on you can work your way down the file, pressing <CTRL-ENTER> on each line of code.

(+ 3 4) and then <CTRL-ENTER>

What Light Table will show you at the end of the line/form, is the result of processing the form (i.e. 7).

Work in Progress
----

This API is very much a work in progress. I started working on this while working through the CMIS and Apache Chemistry in Action book.  So, the amount of the wrapper that is completed varies based on the book and my own uses.  However, it is possible to use the API with the current code.  There are examples in Midje unit tests.

Why Clojure?
----

There are several advantages to using Clojure.  First, it is a Lisp and of course it has a REPL.  This gives us a shell out of the box.  And while there are many Lisps available, Clojure interops with Java, which enables the devloper to rs-use all the existing Java Libraries.  Some would say, "a better java than java".  The other reason for Clojure is wrapping the CMIS API is almost trivial.

However, I admit, the most an important reason to use Clojure is that it is FUN.

Clojure has a highly active group of supporters. 

Resources
---

* [Clojure](http://clojure.org/)
* [Leiningen - Automate Clojure projects without setting your hair on fire](https://github.com/technomancy/leiningen)
* [Clojure-Java Interop](http://clojure.org/java_interop)
* [Midje - Unit Tests](https://github.com/marick/Midje)
* [nREPL](https://github.com/clojure/tools.nrepl)
* [Emacs nREPL.el](https://github.com/kingtim/nrepl.el)
* [A Better Java than Java](http://www.infoq.com/presentations/Clojure-Java-Interop)

## License

Copyright © 2013,2014 Zia Consulting, Inc.

Distributed under the Eclipse Public License, the same as Clojure.