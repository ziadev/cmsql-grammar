(ns chem100.prototype.parsetree
  (:require [chem100.prototype.instaparse :as parser]))
(use '[clojure.string :only (join split)])

(defn parse-character-set [character-set]
  (first (:content (first (:content character-set)))))

(defn parse-content [content]
  (first (:content content)))

(defn parse-value [value]
  (let [content (map :content (:content value))
        count (count content)
        quoted (list (str (first (second content))))]
    (if (= count 3) (first quoted) (first (first content)))))

(defn list->keyword [string]
  (keyword (first string)))

(defn parse-path-or-objectid [path-or-objectid]
  (first (:content (second path-or-objectid))))

(defn parse-columns [columns]
  (flatten (map :content (:content columns))))

(defn parse-assignment [assignment]
  (let [content (:content assignment)
        column-name (parse-content (first content))
        value (parse-value (nth content 2))]
    {:column-name column-name :value value}))

(defn parse-assignments [assignments]
  (loop [result ()
         assignments assignments]
    (if (not (seq assignments)) result
      (recur (conj result (parse-assignment (first assignments))) (rest assignments)))))

(defn parse-connect [query-tag query-content]
  (let [first (first query-content)
        second (second query-content)
        url (parse-character-set (nth query-content 2))
        fourth (nth query-content 3)
        user (parse-character-set (nth query-content 4))
        fifth (nth query-content 5)
        password (parse-character-set (nth query-content 6))
        count (count query-content)
        sixth (if (> count 7) (nth query-content 7))
        connect-name (if (> count 8) (parse-character-set (nth query-content 8)))]
  {:query-tag query-tag :first first :second second :url url :fourth fourth :user user :fifth fifth :password password :count count :sixth sixth :connect-name connect-name}))

(defn parse-update [query-tag query-content]
  (let [first (first query-content)
        table-name (parse-content (second query-content))
        third (nth query-content 2)
        assignments (parse-assignments (:content (nth query-content 3)))]
  {:query-tag query-tag :first first :table-name table-name :third third :assignments assignments}))

(defn parse-select [query-tag query-content]
  (let [first (first query-content)
        column-names (flatten (map :content (:content (second query-content))))
        third (nth query-content 2)
        table-name (parse-content (nth query-content 3))]
  {:query-tag query-tag :first first :column-names column-names :third third :table-name table-name}))

(defn parse-values [values]
  (:content values))

(defn parse-insert [query-tag query-content]
  (let [first (first query-content)
        insert-type (list->keyword (:content (second query-content)))
        third (nth query-content 2)
        path-or-objectid-tag (:tag (nth query-content 3))
        path-or-objectid (parse-path-or-objectid (:content (nth query-content 3)))
        columns (parse-columns (nth query-content 4))
        values (parse-values (:content (nth query-content 5)))
        ]
  {:query-tag query-tag :first first :insert-type insert-type :third third :path-or-objectid-tag path-or-objectid-tag :path-or-objectid path-or-objectid :columns columns :values values}))

(defn parse-query-content [query-tag query-content]
  (cond
   (= query-tag :connect_query) (parse-connect query-tag query-content)
   (= query-tag :update_query) (parse-update query-tag query-content)
   (= query-tag :select_query) (parse-select query-tag query-content)
   (= query-tag :insert_query) (parse-insert query-tag query-content)
   (= query-tag :delete_query) query-content
  ))

(defn parse-tree [statement]
  (let [parsed-statement (parser/sql-parser statement)
        command-tag (:tag parsed-statement)
        command-content (:content parsed-statement)
        moddata-or-util (first command-content)
        moddata-or-util-tag (:tag moddata-or-util)
        moddata-or-util-content (:content moddata-or-util)
        query (first moddata-or-util-content)
        query-tag (:tag query)
        query-content (:content query)
        ]
    (parse-query-content query-tag query-content)))
