(ns chem100.alfrescoSummit2013.demo003
  (:require [chem100.ql :as ql]))
(use '[clojure.string :only (join split)])

(ql/connect "connect to http://localhost:8081/inmemory/atom user admin password admin" "inmem")
(ql/connect "connect to http://localhost:8080/alfresco/cmisatom user admin password admin")
(keys (deref ql/session_map))

(ql/exec-select "select cmis:name from cmisbook:text" "inmem")
(ql/exec-select "select cmis:name from cmisbook:lyrics" "inmem")
(ql/exec-select "select cmis:name from cmisbook:poem" "inmem")

(ql/exec-select "select dpp:lastName from dpp:person where dpp:firstName = 'Mark'")

(ql/exec-select "select dpp:manuscriptNumber from dpp:article")
(count (ql/exec-select "select dpp:manuscriptNumber from dpp:article"))
(map vals (ql/exec-select "select dpp:manuscriptNumber from dpp:article"))

(flatten (map vals (ql/exec-select "select dpp:manuscriptNumber from dpp:article")))
(join ", " (flatten (map vals (ql/exec-select "select dpp:manuscriptNumber from dpp:article"))))
(spit  "./src/chem100/alfrescoSummit2013/summit.txt" (join ", " (flatten (map vals (ql/exec-select "select dpp:manuscriptNumber from dpp:article")))))

(ql/exec-select "select dpp:manuscriptNumber,dpp:status from dpp:article where dpp:status = 'artRejected'")
(ql/exec-select "select cmis:path from dpp:article where dpp:manuscriptNumber = 1236503")
(flatten (map vals (ql/exec-select "select cmis:path from dpp:article")))
(ql/exec-select "select dpp:abstractText from dpp:article where dpp:manuscriptNumber = 1236503")
;(ql/exec-select "select dpp:coverLetter from dpp:article where dpp:manuscriptNumber = 1236503")
(ql/exec-select "select dpp:coverLetterText from dpp:article where dpp:manuscriptNumber = 1236503")
;(ql/exec-select "select dpp:volume from dpp:article where dpp:manuscriptNumber = 1236503")

(ql/exec-select "SELECT dpp:manuscriptNumber FROM dpp:article where dpp:manuscriptNumber like '123%'")
(ql/exec-select "select dpp:manuscriptNumber from dpp:article where dpp:manuscriptNumber != 1236503")
(flatten (map vals (ql/exec-select "select dpp:manuscriptNumber from dpp:article where dpp:manuscriptNumber like '%123%'")))
(flatten (map vals (ql/exec-select "select dpp:manuscriptNumber from dpp:article where dpp:manuscriptNumber like '124%'")))
(flatten (map vals (ql/exec-select "select dpp:status from dpp:article where dpp:status like '%Re%'")))
(flatten (map vals (ql/exec-select "select dpp:status from dpp:article")))

;; "distinct"
(into #{} (flatten (map vals (ql/exec-select "select dpp:status from dpp:article"))))

















