(ns chem100.alfrescoSummit2013.demo004
  (:require [chem100.ql :as ql]))
(use '[clojure.string :only (join split)])

(ql/connect "connect to http://localhost:8081/inmemory/atom user admin password admin" "inmem")
(ql/connect "connect to http://localhost:8080/alfresco/cmisatom user admin password admin")

;; in-memory
(ql/exec-insert "insert folder into path '/' (cmis:name, cmis:objectTypeId) values ('A Testing Folder','cmis:folder')" "inmem")

;; Alfresco
(ql/exec-insert "insert folder into path '/' (cmis:name, cmis:objectTypeId) values ('A Testing Folder','cmis:folder')")
(ql/exec-insert "insert folder into path '/A Testing Folder' (cmis:name, cmis:objectTypeId, dpp:collectionType) values ('ATestFolder2', 'F:dpp:collection' , 'productApproved')")
(ql/exec-insert "insert folder into path '/A Testing Folder' (cmis:name, cmis:objectTypeId, dpp:collectionType) values ('Test', 'F:dpp:collection', 'productUnapproved')")
(ql/exec-insert "insert document into path '/A Testing Folder' (cmis:name, cmis:objectTypeId) values ('Hello World', 'cmis:document')")
(ql/exec-insert "insert document into path '/A Testing Folder' (cmis:name, cmis:objectTypeId, dpp:personId) values ('HelloPerson', 'D:dpp:person', 'mstang@ziaconsulting.com')")

(ql/exec-insert
 "insert file into path '/A Testing Folder' (cmis:name, cmis:objectTypeId, filename, mimeType) values ('purchase_order1.pdf', 'cmis:document', 'purchase_order1.pdf', 'application/pdf')")
(ql/exec-insert
 "insert file into path '/A Testing Folder' (cmis:name, cmis:objectTypeId, filename, mimeType) values ('project.clj', 'cmis:document', 'project.clj', 'text/plain')")
(ql/exec-insert
 "insert file into path '/A Testing Folder' (cmis:name, cmis:objectTypeId, filename, mimeType) values ('Stang.jpg', 'cmis:document', '/home/mstang/Downloads/Stang.jpg', 'image/jpeg')")
