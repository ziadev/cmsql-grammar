(ns chem100.alfrescoSummit2013.demo005
  (:require [chem100.ql :as ql]))
(use '[clojure.string :only (join split)])

(ql/connect "connect to http://localhost:8081/inmemory/atom user admin password admin" "inmem")
(ql/connect "connect to http://localhost:8080/alfresco/cmisatom user admin password admin")

(ql/exec-select "select dpp:middleName,dpp:firstName from dpp:person where dpp:firstName = 'Mark'")
(ql/exec-update "update dpp:person set dpp:middleName = 'Aflresco Summit 2003', dpp:firstName = 'Mark' where dpp:firstName = 'Mark'")
(ql/exec-select "select dpp:middleName,dpp:firstName from dpp:person where dpp:firstName = 'Mark'")
